<?php
// Utility: pretty var_dump
function pre_dump($var) {
	echo "<pre>";
	echo htmlspecialchars(var_export($var,true));
	echo "</pre>";
}

// Sorting functions
function compareOuterLen($a, $b) {
	return compareStrLen($b['outerHtma'],$a['outerHtma']);
}
function compareStrLen($a, $b) {
	return strlen($a)>strlen($b);
}

// Libraries for parsing and output
require_once('simple_html_dom.php');
require_once('Underscore.php/underscore.php');

// Get and cache data
require_once('get_data.php');

class AOMtext {
	// Proxy class so PHP doesn't throw a fit about methods a string doesn't have

	private $obj;

	function __construct($obj) {
		$this->obj = $obj;
	}

	// Accessed on child nodes while rendering template, just return falsy
	public function getTag() {
		return "";
	}

	// Mapped to eachother, just returns the text
	public function getInstance($children=true) {
		return $this->html();
	}
	public function htma($tabs=0) {
		return HTMA_tabulate($this->html(), $tabs+1);
	}
	public function html($template=false,$framework=false) {
		return $this->obj;
	}
};
class AOMnode {

	private $obj;

	function __construct($obj) {
		$this->obj = $obj;
	}

	// Get properties in htma or html strings, or return from obj
	function getTag($mode=false) {
		return $this->obj['tagName'];
	}
	function getId($mode=false) {
		if ($mode == 'htma')
			return $this->obj['id'] ? '#'.$this->obj['id'] : '';
		elseif ($mode == 'html')
			return $this->obj['id'] ? ' id="'.$this->obj['id'].'"' : '';
		else return $this->obj['id'];
	}
	function getClass($mode=false) {
		if ($mode == 'htma')
			return $this->obj['class'] ? '.'.implode('.', $this->obj['class']) : '';
		elseif ($mode == 'html')
			return $this->obj['class'] ? ' class="'.implode(' ', $this->obj['class']).'"' : '';
			
		else return $this->obj['class'];
	}
	function getAttr($mode=false) {
		if ($mode == 'htma' || $mode == 'html') {
			if (! $this->obj['attr'])
				return '';
			$ret = array();
			foreach($this->obj['attr'] as $attr=>$val)
				$ret[]="$attr=\"$val\"";
			return ' '.implode(' ', $ret);
		}
		else return $this->obj['attr'];
	}

	// Output parsed attributes using $HTMA_attr_trans
	public function getInstance($children=true, $parseFlag = false) {
		global $HTMA_attr_trans;
		$obj = array("tagName"=>$this->getTag());

		if ($this->getId())
			$obj["id"]=$this->getId();
		if ($this->getClass())
			$obj["class"]=$this->getClass();

		if ($this->getAttr()) {
			foreach ($this->getAttr() as $prop=>$val) {
				if ($prop == 'class') {
					$cl = preg_split("/\s+/m", $val);
					if (array_key_exists("class",$obj))
						$obj["class"]=array_merge($obj["class"], $cl);
					else $obj["class"] = $cl;
				} else $obj[$prop] = $val;
			}
		}
		// Parse custom attributes (position, options...)
		foreach ($obj as $prop=>$val) {

			$obj[$prop] = HTMA_replaceInlineVars($val);
			
			if (array_key_exists($prop, $HTMA_attr_trans)) {
				$n_val = HTMA_funct($prop, $obj);
				if (!empty($n_val))
					$obj[$prop] = $n_val;
				else unset($obj[$prop]);
			}
		}
		if ($parseFlag)
			$obj["attrParsed"] = true;

		// Get children
		if ($children) {
			if (array_key_exists('childNodes', $this->obj) && $this->obj['childNodes']) {
				if ($children === 'node')
					$obj['childNodes'] = $this->obj['childNodes'];
				else {
					$obj['childNodes'] = array();
					foreach ($this->obj['childNodes'] as $child)
						$obj['childNodes'][] = $child->getInstance($children, $parseFlag);
				}
			}
			else $obj['childNodes'] = array();
		}
		return $obj;
	}

	// Output node as string
	public function htma($tabs=0) {
		$tag = str_repeat("\t",$tabs)."<{$this->getTag('htma')}{$this->getId('htma')}{$this->getClass('htma')}{$this->getAttr('htma')} >";
		foreach ($this->obj['childNodes'] as $child) {
			if (!is_string($child))
				$tag.="\n".$child->htma($tabs+1);
			else $tag.="\n".HTMA_tabulate($child, $tabs+1);
		}
		return $tag;
	}
	public function html($template = false, $framework="bootstrap") {
		$obj = $this->getInstance('node');
		return HTMA_convertNode($obj, $template, $framework);
	}
};

class HTMA {
	function __construct() {
	}

	// Output so many things
	public function dump($input, $dump=false) {
		$args = HTMA_getFunctArgs(func_get_args());
		$print = array();
		foreach ($args["input"] as $child) {
			if (!is_string($child))
				$print[] = $child->getInstance(true,true);
			else $print[] = $child;
		}
		if ($args["dump"])
			pre_dump($print);
		return $print;
	}
	public function htma($input, $dump=false) {
		$args = HTMA_getFunctArgs(func_get_args());
		$print = array();
		foreach ($args["input"] as $child) {
			if (!is_string($child))
				$print[] = $child->htma();
			else $print[] = $child;
		}
		$htma = implode("\n",$print);
		if ($args["dump"])
			pre_dump($htma);
		return $htma;
	}
	public function json($input, $dump=false) {
		$args = HTMA_getFunctArgs(func_get_args());
		$print = json_encode($this->dump($args["input"]));
		if ($args["dump"])
			pre_dump($print);
		return $print;
	}

	public function html($input, $data=null, $framework=null, $dump=null) {
		$args = HTMA_getFunctArgs(func_get_args());
		if ($args["data"]) {
			$pass = array();
			$pass[] = $args["input"];
			$pass[] = $args["framework"];
			$pass[] = $args["data"];
			return call_user_func_array(array($this, "template"), $pass);
		}
		foreach ($args["input"] as $child) {
			if (!is_string($child))
				$print[] = $child->html(false, $args['framework']);
			else $print[] = $child;
		}
		$html = implode("\n",$print);
		if ($args["dump"])
			pre_dump($html);
		return $html;
	}
	public function template($input, $data=null, $framework=null, $dump=null) {
		$args = HTMA_getFunctArgs(func_get_args());
		$pass = array();
		$pass[] = $args["input"];
		$pass[] = $args["framework"];
		$html = call_user_func_array(array($this, "html"), $pass);
		$temp = __::template($html);
		if ($args["data"])
			$temp = $temp($args["data"]);
		if ($args["dump"])
			pre_dump($temp);
		return $temp;
	}
	public function dom($input, $data=null, $framework=null, $dump=null) {
		$args = HTMA_getFunctArgs(func_get_args());
		$pass = array();
		$pass[] = $args["input"];
		$pass[] = $args["framework"];
		if ($args["data"])
			$pass[] = $args["data"];
		$html = call_user_func_array(array($this, "html"), $pass);
		$dom = str_get_html($html);
		if ($args["dump"])
			pre_dump($dom);
		return $dom;
	}

	// Utility outputer, maps $mode to the above functions
	public function get($input, $mode='html', $framework=null, $data=null, $dump=null) {
		$args = HTMA_getFunctArgs(func_get_args());
		if	($args['mode'] == 'htma')
			return $this->htma($args['input'], $args['dump']);
		elseif ($args['mode'] == 'json')
			return $this->json($args['input'], $args['dump']);
		elseif ($args['mode'] == 'dump')
			return $this->dump($args['input'], $args['dump']);

		elseif ($args['mode'] == 'html')
			return $this->html($args['input'], $args['framework'], $args['data'], $args['dump']);
		elseif ($args['mode'] == 'dom')
			return $this->dom($args['input'], $args['framework'], $args['data'], $args['dump']);
		elseif ($args['mode'] == 'template')
			return $this->template($args['input'], $args['framework'], $args['data'], $args['dump']);
		else die("Format not recognised: {$mode};\nUse html, htma, json, dom, dump or template.");
	}
};


// Utility
function str_get_htma($htma, $return="html", $framework=null, $data=null) {
	$A = new HTMA();
	return call_user_func_array(array($A,"get"), func_get_args());
}
function file_get_htma($fileName, $return="html", $framework=null, $data=null) {
	$htma = file_get_contents($fileName);
	return str_get_htma($htma, $return, $framework, $data);
}

// If template given AND (compile flag set OR this is main script)
//	Then parse as request
if (isset($_REQUEST['template']) &&
		(isset($_REQUEST['compile']) || count(debug_backtrace()) == 0 || basename($argv[0]) == basename(__FILE__)))
{
	include('parse_request.php');
}
