var scripts= document.getElementsByTagName('script');
var path= scripts[scripts.length-1].src.split('?')[0];      // remove any ?query
var _DIR= path.split('/').slice(0, -1).join('/')+'/';  // remove last filename part of path

function loader(callback) {
	var count_in = 0,
		count_out = 0,
		count_down = function () {
			count_out += 1;
			if (count_in == count_out)
				callback();
		};
	return function /*loadjscssfile*/(filename, filetype){
		count_in += 1;
		if (filetype===undefined)
			filetype = filename.split('.').slice(-1)[0];
		//http://www.javascriptkit.com/javatutors/loadjavascriptcss.shtml
		if (filetype=="js"){ //if filename is a external JavaScript file
			var fileref=document.createElement('script')
			fileref.setAttribute("type","text/javascript")
			fileref.setAttribute("src", filename)
		}
		else if (filetype=="css"){ //if filename is an external CSS file
			var fileref=document.createElement("link")
			fileref.setAttribute("rel", "stylesheet")
			fileref.setAttribute("type", "text/css")
			fileref.setAttribute("href", filename)
		}
		if (typeof fileref!="undefined") {
			fileref.onload=count_down;
			document.getElementsByTagName("head")[0].appendChild(fileref)
		}
	}
}
window.HTMA_IS_READY = false;
window.HTMA_READY_CALLBACKS = [];
var file_loader = loader(function () {
	$(function () {
		window.HTMA_IS_READY = true;
		for (var c in window.HTMA_READY_CALLBACKS)
			window.HTMA_READY_CALLBACKS[c]();
	})
})
file_loader(_DIR+'data/htma.function', 'js');
file_loader(_DIR+'data/template.function', 'js');


var trim = (function () {
    "use strict";
//http://codereview.stackexchange.com/questions/28464/trim-certain-characters-from-a-string-in-javascript
    function escapeRegex(string) {
        return string.replace(/[\[\](){}?*+\^$\\.|\-]/g, "\\$&");
    }

    return function trim(str, characters, flags) {
        flags = flags || "g";
        if (typeof str !== "string" || typeof characters !== "string" || typeof flags !== "string") {
            throw new TypeError("argument must be string");
        }

        if (!/^[gi]*$/.test(flags)) {
            throw new TypeError("Invalid flags supplied '" + flags.match(new RegExp("[^gi]*")) + "'");
        }

        characters = escapeRegex(characters);

        return str.replace(new RegExp("^[" + characters + "]+|[" + characters + "]+$", flags), '');
    };
}());
if (!String.prototype.format) {
//http://stackoverflow.com/questions/18405736/is-there-a-c-sharp-string-format-equivalent-in-javascript
  String.prototype.format = function() {
	var args = arguments;
	return this.replace(/\%(\d+)\$s/g, function(match, number) {
	  return typeof args[number-1] != 'undefined'
		? args[number-1]
		: match
	  ;
	});
  };
}
function isJSON (jsonString){
//http://stackoverflow.com/questions/3710204/how-to-check-if-a-string-is-a-valid-json-string-in-javascript-without-using-try/3710226
	try {
		var o = JSON.parse(jsonString);
		if (o && typeof o === "object" && o !== null) {
			return o;
		}
	}
	catch (e) { }

	return false;
};

window.AOMAttr = {
	abbrTrans:{
	},
	nodeRegex:{
	}
};

function strlen(str) {
	return str.length;
}
function str_replace(REPLACE, WITH, IN) {
	return IN.split(REPLACE).join(WITH);
}
function str_repeat(str, repeat) {
	if (repeat === undefined)repeat=1;
	var ret= "";
	for (var r = 0;r<repeat;r++)
		ret+=str;
	return ret;
}
function var_dump () {
	console.log.call(arguments);
}
function preg_match_all(regex, str, ret) {
	var glob=new RegExp(regex.source, regex.toString().split('/').slice(-1)[0]+"g"),
		matches = str.match(glob);
	if (!matches) return [[]];
	if (ret === undefined)
		ret=[];
	var match,i=0,
		stop=matches.length;
	while ((match = glob.exec(str)) && i<stop) {
		for (var m in match)
			if (! isNaN(m)) {
				if (!(m in ret))
					ret[m] = [];
				ret[m].push(match[m]);
			}
		i++;
	}
	return ret;
}
function explode(a,b) {
	return b.split(a);
}
function implode(a,b) {
	return b.join(a);
}
function call_user_func_array(funct, args) {
	return funct.apply(window, args);
}

function compareOuterLen($a, $b) {
	return compareStrLen($b['outerHtma'],$a['outerHtma']);
}
function compareStrLen(a,b) {
	return Math.sign(a.length - b.length);
}

function parseRegex (reg) {
	var replaced = false;
	while (!replaced) {
		for (var r in reg) {
			for (var R in reg)
				reg[r] = reg[r].replace('$'+R+'$', reg[R]);
		}
		replaced = true;
		for (var r in reg) {
			if (reg[r].match(/\$[a-z]+\$/)) {
				replaced = false;
				break;
			}
		}
	}
	reg.tagline = "^(\\t*)"+reg.tagline;
	var glob = ['attributes','end','start'];
	var flags = {
		attributes:'',
		end:'g',
		start:'g',
		king:'m',
	};
	for (var r in reg) {
//		console.log(reg[r])
//		while (reg[r].indexOf('(?:\\n)')!=-1)
//			reg[r] = reg[r].replace('(?:\\n)',"\\n+\\r*");
		if (r in flags)
			reg[r] = new RegExp(reg[r], flags[r]);
		else reg[r] = new RegExp(reg[r]);
	}
	return {
		"regex":reg,
		"groups":HTMA_getRegexGroups(reg["king"])
	};
}





function AOMtext(options){
	this.text = "";
	this.setOptions = function (opts) {
		this.text = opts;
	}
	this.html = function () {
		return this.text;
	}
	this.setOptions(options);
}
function AOMnode(options) {
	this.childNodes = [];
	this.tabLevel = 0;
	this.tagName = 'div';
	this.attrParsed = false;
	this.appendNode = function (node) {
		node.parentNode = this;
		this.childNodes.push(node)
	}
	this.setOptions = function (opts) {
		if (opts === undefined) return;
		if ('attr' in opts) {
			for (var o in opts['attr'])
				this[o] = opts['attr'][o];
			delete opts['attr'];
		}
		for (var o in opts)
			this[o] = opts[o];
	}
	this.getInstance = function (children, parseFlag) {
		if (children === undefined)
			children = false;
		if (parseFlag === undefined)
			parseFlag = false;
		var properties = {};

		for (var p in this) {
			// casetype to get attributes
			if (p.toLocaleLowerCase()==p && typeof this[p] != 'function')
				properties[p] = this[p];
		}
		if (! this.attrParsed){
			for (var p in properties) {
				properties[p] = HTMA_replaceInlineVars(properties[p]);
				if (p in window.AOMAttr)
					properties[p] = window.AOMAttr._funct(p, properties);
			}
		}
		if (children) {
			var childs = [];
			for (var c in this.childNodes) {
				var child = this.childNodes[c];
				if (typeof child != 'string')
					childs.push(child.getInstance(true,true));
				else childs.push(child);
			}
			properties.childNodes = childs;
		} else properties.childNodes = this.childNodes;

		properties.tagName = this.tagName;

		if (parseFlag)
			properties.attrParsed = true;

		return properties;
	}
	this.html = function (template, framework) {
		var $obj = this.getInstance();
		return HTMA_convertNode($obj, template, framework);
	}
	this.getId = function () {
		if ('id' in this)
			return "#"+this.id;
		else return "";
	}
	this.getClass = function () {
		if ('class' in this && this.class)
			return "."+this.class.join('.');
		else return "";
	}
	this.getAttr = function () {
		return "";
//		if ('class' in this)
//			return "."+this.class.join('.');
//		else return "";
	}
	this.htma = function (tabs) {
		if (tabs === undefined)
			tabs = 0;

		var tag = str_repeat("\t",tabs)+"<"+this.tagName+this.getId('htma')+this.getClass('htma')+this.getAttr('htma')+" >";
		for (var c in this.childNodes) {
			var child = this.childNodes[c];
			if (typeof child != 'string')
				tag+="\n".child.htma(tabs+1);
			else tag+="\n".HTMA_tabulate(child, tabs+1);
		}
		return tag;
	}
	this.setOptions(options);
}

function HTMA(options) {

	var _this = this,
		regex={};
	if (options === undefined)
		options = {};

	this.init = function() {
		$.get(_DIR+'data/htma.json',function (data) {
			window.AOMAttr = $.extend(window.AOMAttr, data);
			_this.ready();
		},'json').fail(function (data) {
			_this.data_ready();
		});
	}


	this.json = function () {
		var args = HTMA_getFunctArgs(arguments);
		var print = JSON.stringify(this.dump(args.input));
		if (args["dump"])
			console.log(print);
		return print;
	}
	this.dump = function () {
		var args = HTMA_getFunctArgs(arguments);
		var print = [];
		for (var c in args.input) {
			var child = args.input[c];
			if (typeof child != 'string')
				print.push(child.getInstance(true,true));
			else print.push(child);
		}
		if (args["dump"])
			console.log(print);
		return print;
	}
	this.htma = function () {
		var args = HTMA_getFunctArgs(arguments);
		var ret = "";
		for (var n in args.input) {
			var node = args.input[n];
			if (typeof node == 'string')
				ret+=node;
			else ret+=node.htma();
		}
		if (args.dump)
			console.log(ret);
		return ret;
	}

	this.html = function () {
		var args = HTMA_getFunctArgs(arguments);
		if (args.data) {
			var pass = [args['input'], args['framework'], args['data']];
			return this.template.apply(this, pass);
		}
		var ret = "";
		for (var n in args.input) {
			var node = args.input[n];
			if (typeof node == 'string')
				ret+=node;
			else ret+=node.html(false, args['framework']);
		}
		if (args.dump)
			console.log(ret);
		return ret;
	}
	this.template = function () {
		var args = HTMA_getFunctArgs(arguments);
		var pass = [args['input'], args['framework']];
		var temp = _.template(this.html.apply(this, pass));
		if (args.data)
			temp = temp(args.data);
		if (args.dump)
			console.log(temp);
		return temp;
	}
	this.dom = function () {
		var args = HTMA_getFunctArgs(arguments);
		var pass = [args['input'], args['framework'], args['data']];
		if (args.data)
			pass.push(args.data);
		var html = this.html.apply(this, pass);
		var dom = $('<div>')
			.html(html)
			.contents();
		if (args.dump)
			console.log(dom);
		return dom;
	}

	// Utility jquery append
	this.render = function (template, $el, format) {
		var pass = arguments;
		if (format === undefined)
			format = 'html';
		if (pass[2] != format)
			pass[2] = format;
		if ($el === undefined) $el = $('body');
		if (1 in pass)
			delete pass[1];
		var args = HTMA_getFunctArgs(pass);
		// not sure how to handle this with framework and data
		//	perhaps pass a spliced args to getFunctArgs
		console.log(args.mode,_this[args.mode]);
		var $els = _this[args.mode].apply(_this, pass);
		$el.append($els);
		return $els;
	}

	// Make sure all files, config data and templates are loaded
	this.compileTemplates = function (temps) {
		var prefix = 'htma-',
			templates = {};
		temps.each(function () {
			var $this = $(this),
				$ids = $this.attr('id').slice(prefix.length),
				$ids_l = $ids.split('-'),
				$name = $ids_l.slice(1).join('-'),
				$framework = $ids_l[0],
				$temp = _.template($this.html().replace(/\s+/g,' '));
			if (!($framework in templates))
				templates[$framework] = {};
			templates[$framework][$name] = $temp;
		});
		window.$HTMA_templates = templates;
		options.templates = templates;
		if (! Object.keys(templates).length)
			console.log( "There are no templates in ",temps);
		this.temp_ready();
	}
	this.ready = function () {
		if (!window.HTMA_IS_READY) {
			window.HTMA_READY_CALLBACKS.push(_this.ready);
			return;
		}
		regex = parseRegex(window.AOMAttr.nodeRegex);
		window.$HTMA_regex = regex;
		_this.data_ready();
	}
	this.temp_ready = function () {
		var $appends = $('[type="text/htma"][appendTo]');
		$appends.each(function () {
			var $this = $(this),
				$appendTo = $this.attr('appendTo'),
				$framework = $this.attr('framework'),
				$format = $this.attr('format'),
				$data = $this.attr('data'),
				$dump = $this.attr('dump');
			if (!$appendTo) $appendTo = 'body';
			var $pass = [$($appendTo)];
			if ($format)
				$pass.push($format);
			else $pass.push('html');
			if ($framework)
				$pass.push($framework);
			if ($data)
				$pass.push($data);
			if ($dump)
				$pass.push($dump);
			if ($this.is('[href]')) {
				$.get($this.attr('href'),function(data){
					$pass.splice(0,0,data);
					_this.render.apply(_this, $pass);
				});
			} else {
				$pass.splice(0,0,$this.html());
				_this.render.apply(_this, $pass);
			}
		});
		if ('render' in options) {
			if (typeof options.render == 'function')
				options.render(_this.render);
			else _this.render(options.render);
		}
	}
	this.data_ready = function () {

		if (! ('framework' in options))
			options.framework = "default";

		var prefix = 'htma-',
			selector = 'script[name=htma][id^="'+prefix+'"]';

		if ('templates' in options)
			_this.compileTemplates(options.templates);
		else {
			var tmp_els = $(selector);
			if (tmp_els.length)
				_this.compileTemplates(tmp_els);
			else {
				$.get(_DIR+'data/templates.temp.php', function (tmphtml) {
					var $tmp = $('<div>',{style:'display:none;'});
					$('body').append($tmp);
					$tmp.html(tmphtml);
					_this.compileTemplates($tmp.find(selector));
					$tmp.remove();
				});
			}
		}
	}

	this.init();
}
