A simpler way to write markup, HTMA uses tabulation to close elements, removing
the need for closing tags as well as a few other abbreviations of HTML eg. ID and classes can be set using CSS notation