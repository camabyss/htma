<?php

$HTMA_phpvar_regex="(\\$[a-zA-Z_]+(\\[([a-zA-Z0-9\\\"\\'\\[\\]_\\$-]+)\\])?)";
$HTMA_php_regex = json_decode(file_get_contents(__DIR__."/data/functDecode.json"), true);
# get data
function HTMA_getData($d) {
	global $HTMA_phpvar_regex;
	global $HTMA_php_regex;

	$dir = __DIR__."/data/$d";
	$json = "$dir.json";
	$funct = "$dir.function";
	$php = "$funct.php";
	$json_e = file_exists($json);
	$funct_e = file_exists($funct);
	$php_e = file_exists($php);
	
	if ($json_e) {
		$JSON = file_get_contents($json)
			or die("Could not get file contents: $json");
		if ($JSON)
			$JSON = json_decode($JSON, true);
		if (!$JSON)
			die("Could not decode JSON: $json");
	} else die("Could not find file: $json");
	if ($funct_e) {
		if ($php_e && (! isset($_REQUEST['dev']))) {
			if (filemtime($php) < filemtime($funct) ||
				($json_e && filemtime($php) < filemtime($json)))
				$DECODE = true;
			else $DECODE = false;
		} else $DECODE = true;
		if ($DECODE) {
			$FUNCT = file_get_contents($funct)
				or die("Could not get file contents: $funct");
			if ($FUNCT) {
				if (array_key_exists('functDecode',$JSON)) {
					foreach ($JSON['functDecode'] as $regx=>$replace)
						$FUNCT = preg_replace(str_replace('$phpvar', $HTMA_phpvar_regex, $regx), $replace, $FUNCT);
					foreach ($HTMA_php_regex['functDecode'] as $regx=>$replace)
						$FUNCT = preg_replace(str_replace('$phpvar', $HTMA_phpvar_regex, $regx), $replace, $FUNCT);
//					var_dump($FUNCT);
					$FUNCT = "<?php ".$FUNCT;
					if ((! $php_e) || $FUNCT != file_get_contents($php))
						file_put_contents($php, $FUNCT);
				}
			}
		}
	} else die("Could not find file: $funct");
	require_once($php);
	return $JSON;
}

function HTMA_getTemplates($mode='cache') {
	global $HTMA_phpvar_regex;
	global $HTMA_php_regex;
	global $template_abbr;

	$dir = __DIR__."/data";
	$json = "$dir/template.json";
	$base = "$dir/templates.temp";
	$temp = "$base.php";
	$cache = "$temp.cache";

	if ($mode == 'php')
		$temp = $cache;

	if (file_exists($temp)) {
		if ($mode == 'cache') {
			if (! isset($_REQUEST['dev'])) {
				$COMPILE = false;
				if (file_exists($cache)) {
					if (filemtime($cache) < filemtime($temp) || filemtime($cache) < filemtime($json))
						$COMPILE = true;
				} else $COMPILE = true;
				error_log($COMPILE);
			} else $COMPILE = true;
			if (!$COMPILE)
				return HTMA_getTemplates('php');
		}
		ob_start();
		include($temp);
		$temp_str = ob_get_contents();
		ob_end_clean();
		$template = str_get_html($temp_str);
		$templates = array();
		$els = array();
		foreach($template->find('script[name=htma]') as $element) {
			$ids = explode('-',substr($element->id, strlen('htma-')));
			$fw = $ids[0];
			$id = implode('-', array_slice($ids, 1));
			$tmp = $element->innertext;
			if ($mode == 'cache' && array_key_exists('functDecode',$template_abbr)) {
				$tmp = preg_replace('/\s*$^\s*/m', "\n", $tmp);
				$tmp = preg_replace('/[ \t]+/', ' ', $tmp);
				foreach ($template_abbr['functDecode'] as $regx=>$replace)
					$tmp = preg_replace(str_replace('$phpvar',$HTMA_phpvar_regex, $regx), $replace, $tmp);
				foreach ($HTMA_php_regex['functDecode'] as $regx=>$replace)
					$tmp = preg_replace(str_replace('$phpvar', $HTMA_phpvar_regex, $regx), $replace, $tmp);
				$els[] = "<script type='text/html' name='htma' id='htma-$fw-$id' >\n$tmp\n</script>";
			} else {
				$tmp = __::template($tmp);
				if (! array_key_exists($fw, $templates))
					$templates[$fw] = array();
				$templates[$fw][$id] = $tmp;
			}
		}
		if ($mode == "cache") {
			$cont = implode("\n", $els);
			file_put_contents($cache, $cont);
			return HTMA_getTemplates('php');
		}
		return $templates;
	}
}

function HTMA_getRegex ($regex, $d='htma') {
	$dir = __DIR__."/data/";
	$json = "$dir$d.json";
	$cache = "$json.regex.php";
	$COMPILE = false;
	if (isset($_REQUEST['dev']) || (! file_exists($cache))) $COMPILE = true;
	else if (filemtime($cache) < filemtime($json))
		$COMPILE = true;
	if ($COMPILE) {
		$replaced = false;
		while (!$replaced) {
			foreach ($regex as $name=>$ret) {
				foreach ($regex as $replaceName=>$replaceRegex)
					$ret = str_replace('$'.$replaceName.'$', $replaceRegex, $ret);
				$regex[$name] = $ret;
			}
			$replaced = true;
			foreach ($regex as $name=>$reg) {
				if (preg_match("/\\$([a-z]+)\\$/",$reg)) {
					$replaced = false;
					break;
				}
			}
		}
		foreach ($regex as $name=>$reg)
			$regex[$name] = "/{$reg}/m";
//		$regex['king'].='g';
		$regex_groups = HTMA_getRegexGroups($regex["king"]);
		$content = "<?php \$HTMA_regex = ".var_export($regex, true)."; \$HTMA_regex_groups = ".var_export($regex_groups, true).";";
		file_put_contents($cache, $content);
		return array("regex"=>$regex,"groups"=>$regex_groups);
	}
	require_once($cache);
	return array("regex"=>$HTMA_regex,"groups"=>$HTMA_regex_groups);
}
/*
function HTMA_getRegexGroups($king) {
	$ret = array();
	$attr = array(
		'tag'=>'test1',
		'id'=>'test2',
		'class'=>'.test3.test4',
		'attr'=>'test5=test6 test7=test8',
		'innerHtma'=>"\t<div>\n\t\ttest9 test10\n\ttest11 test12\n"
	);
	$testHtma ="<{$attr['tag']}#{$attr['id']}{$attr['class']} {$attr['attr']}>\n{$attr['innerHtma']}";
//		$attr['outerHtma'] = $testHtma;
	$match = array();
	preg_match_all("/$king/m",$testHtma,$match);

	foreach ($attr as $at=>$val) {
		foreach ($match as $index=>$a) {
			if ($a[0] == $val) {
				$ret[$at] = $index;
				break;
			}
		}
	}
	return $ret;
}*/

$HTMA_attr_trans = HTMA_getData('htma');

$HTMA_regex = HTMA_getRegex($HTMA_attr_trans["nodeRegex"]);

$template_abbr = HTMA_getData('template');
$HTMA_templates = HTMA_getTemplates();