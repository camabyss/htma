<script id="htma-default-default"
		name="htma" type="text/html" >
	<% var $alt = HTMA_getRequired(arguments[0],['alt']); %>
	<<%- $t %> class="<%- $i('class') ? $g('class') : '' %>
				<%- $i('state') ? $g('state') : '' %>
				<%- $i('column') ? $g('column') : '' %>"
			<%= $alt ? 'alt="'+$alt["alt"]+'"' : '' %>
			<%= $g('rest') %> >
		<%= HTMA_printNode({0:{a:arguments}, 1:'default-inner', "framework":$f}) %>
	</<%- $t %>>
</script>
<script id="htma-default-default-inner"
		name="htma" type="text/html" >
	<% var $alt = HTMA_getRequired(arguments[0],['alt']); %>
	<% if ($i('icon')) { %>
		<i class="fa fa-<%- $g('icon') %>"></i>
	<% } %>
	<% if ($i('label')) { %>
		<span class="<%- $t %>-label"><%- $g('label') %></span>
	<% } %>

	<% _.each($c, function ($child) { %>
		<%= HTMA_printNode({0:$child, "framework":$f}) %>
	<% }); %>

	<% if ($i('badge')) { %>
		<span class="badge"><%- $g('badge') %></span>
	<% } %>
</script>
<script id="htma-default-br"
		name="htma" type="text/html" >
	<br/>
</script>
<?php
include ('functions.temp.php');
include ('bootstrap.temp.html');
include ('ionic.temp.html');
