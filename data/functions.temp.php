<?php
$_cs = '<%- "{{%%" %>';
$_ps = '<%- "{{%%-" %>';
$_rs = '<%- "{{%%=" %>';
$_e = '<%- "%%}}" %>';
?>

<script id="htma-functions-log"
		name="htma" type="text/html" >
		<?php echo $_cs; ?>
			console.log(
				<%= HTMA_printNode({0:{a:arguments}, 1:'inner-args', "framework":$f}) %>
			);
		<?php echo $_e; ?>
</script>

<script id="htma-functions-print"
		name="htma" type="text/html" >
		<?php echo $_ps; ?>
			<%= HTMA_printNode({0:{a:arguments}, 1:'inner-args', "framework":$f}) %>
		<?php echo $_e; ?>
</script>

<script id="htma-functions-raw"
		name="htma" type="text/html" >
		<?php echo $_rs; ?>
			<%= HTMA_printNode({0:{a:arguments}, 1:'inner-args', "framework":$f}) %>
		<?php echo $_e; ?>
</script>



<script id="htma-functions-inner-args"
		name="htma" type="text/html" >
		<%
		var $sep = ' + ';
		if ($t == 'log')
			$sep = ', ';
		%>
		<%= HTMA_printNode({0:{a:arguments}, 1:'var-inner', "framework":$f}) %>
			<% _.each($c, function ($child,$index) {// use($sep) { %>
				<% if (! $child.tagName) { %>
					<%- $index || $i('id') ? $sep : '' %>
					"<%= HTMA_printNode({0:$child, "framework":$f}) %>"
				<% } else if ($child.tagName == 'var') { %>
					<%- $index || $i('id') ? $sep : '' %>
					<%= HTMA_printNode({0:$child, 1:'var-inner', "framework":$f}) %>
				<% } %>
			<% }); %>
</script>

<script id="htma-functions-var"
		name="htma" type="text/html" >
	<?php echo $_cs; ?>
	var <%= HTMA_printNode({0:{a:arguments}, 1:'var-inner', "framework":$f}) %>
		<%- $i('value') ? " = "+$g('value') : '' %>;
	<?php echo $_e; ?>
</script>

<script id="htma-functions-var-inner"
		name="htma" type="text/html" >
	<%- $i('id') ? '$'+$g('id') : ''%>
</script>


<script id="htma-functions-for"
		name="htma" type="text/html" >
	<% if ($i('id')) { %>
		<?php echo $_cs; ?>
		<% if ($i('in')) { %>
			_.each($<%- $g('in') %>, function ($<%- $g('id') %><%= $i('index') ? ', $'+$g('index') : '' %>) {
		<% } else if ($i('stop')) { %>
			for (var $<%- $g('id') %> = <%- $i('start') ? $g('start') : '0' %>;
					$<%- $g('id') %> < <%- $g('stop') %>;
					$<%- $g('id') %>+=1) {
		<% } %>
		<?php echo $_e; ?>

		<% _.each($c, function ($child) { %>
			<%= HTMA_printNode({0:$child, "framework":$f}) %>
		<% }); %>

		<?php echo $_cs; ?>
		<% if ($i('in')) { %>
			});
		<% } else if ($i('stop')) { %>
			}
		<% } %>
		<?php echo $_e; ?>
	<% } %>
</script>