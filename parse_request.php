<?php

$template = $_REQUEST['template'];

if (isset($_REQUEST['format']))
	$format = $_REQUEST['format'];
else $format = 'html';

if (isset($_REQUEST['framework']))
	$framework = $_REQUEST['framework'];
else $framework = "bootstrap";

///*
if (isset($_REQUEST['data']))
	$data = json_decode($_REQUEST['data'], true);
else//*/
$data = false;

//http://stackoverflow.com/questions/16481641/php-regex-match-all-urls
$url_r = '#^[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?$#si';

if (preg_match($url_r, $template) || (strlen($template) < 260 && file_exists($template)))
	$result = file_get_htma($template, $format, $framework, $data);
else $result = str_get_htma($template, $format, $framework, $data);

echo $result;